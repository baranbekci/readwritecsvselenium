package read.from.csv;

import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.opencsv.exceptions.CsvValidationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;

public class ReadingDataFromCsv {
    static String ROOT = System.getProperty("user.dir");
    String CSV_PATH= ROOT + "/personelListesi.csv";

    @Test
    public void csvDataRead() throws IOException, InterruptedException, CsvValidationException {

        CSVReader reader = new CSVReader(new FileReader(CSV_PATH));
        String [] csvCell;
        System.setProperty("webdriver.chrome.driver", ROOT + "/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        //while loop will be executed till the last line In CSV.
        while ((csvCell = reader.readNext()) != null) {

            String email = csvCell[3];

            driver.get("https://yopmail.com/en/");
            driver.findElement(By.xpath("//input[@id='login']")).clear();
            driver.findElement(By.xpath("//input[@id='login']")).sendKeys(email);
            driver.findElement(By.xpath("//div[@id='refreshbut']")).click();
            driver.switchTo().frame("ifmail");
            driver.findElement(By.xpath("//b[contains(text(),'Şifre : ')]")).getText();
            String password = driver.findElement(By.xpath("//b[contains(text(),'Şifre : ')]")).getText();
            System.out.println(password);
            WriteToCsv.writeToCsv(email + ", " + password + " " + new Date());
            driver.switchTo().defaultContent();
        }
        driver.quit();
    }
}
